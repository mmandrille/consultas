from django.db import models
#Extra modules import
from tinymce.models import HTMLField
#Import propios
from core.api import obtener_organismos, nombre_organismo

# Create your models here.
class Consulta(models.Model):
    autor = models.CharField('Nombre y Apellido', max_length=100)
    email = models.EmailField('Correo Electronico Personal')
    telefono = models.CharField('Telefono', max_length=50, blank=True, null=True)
    organismo = models.PositiveIntegerField(choices= obtener_organismos(), default=0)
    asunto = models.CharField('Asunto', max_length=100)
    descripcion = HTMLField()
    fecha_consulta = models.DateTimeField(auto_now_add=True)
    valida = models.BooleanField('Email Validado', default=False)
    def __str__(self):
        return (nombre_organismo(self.organismo) + ": " +self.asunto + '(' + str(self.fecha_consulta.date()) + ')')