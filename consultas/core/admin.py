from django.contrib import admin
#Import Personales
from .models import Consulta

#Definicions
class ConsultaAdmin(admin.ModelAdmin):
    list_filter = ['valida',]# 'organismo']

# Register your models here.
admin.site.register(Consulta, ConsultaAdmin)