#Import Standard
from django import forms
from django.forms import ModelForm

#Import del Proyecto
from .models import Consulta

#Definimos nuestros Formularios
class BuscarForm(forms.Form):
    texto = forms.CharField(
        max_length=50,
        min_length=5,
        help_text='',
        label='Buscar Organismo',
        widget=forms.TextInput(attrs={'placeholder': 'Nombre Organismo', 'autofocus': 'autofocus'})
    )

class ConsultaForm(ModelForm):
    class Meta:
        model = Consulta
        fields = ['autor', 'email', 'telefono', 'asunto', 'descripcion', 'organismo']
        widgets = {'organismo': forms.HiddenInput()}