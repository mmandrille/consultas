from django.shortcuts import render
from django.template.loader import render_to_string
from django.core.mail import EmailMessage
#Import personales
from .models import Consulta
from core.api import obtener_organismo, obtener_organismos
from .forms import BuscarForm, ConsultaForm
from .tokens import account_activation_token

# Create your views here.
def home(request):
    organismos = obtener_organismos()
    if request.method == 'POST': #En caso de que se haya realizado una busqueda
        buscar_form = BuscarForm(request.POST)
        if buscar_form.is_valid():
            #Realizamos el filtrado
            organismos = [org for org in organismos if buscar_form.cleaned_data['texto'].lower() in org[1].lower()]
    else:
        buscar_form = BuscarForm()
    return render(request, 'home.html', {'organismos': organismos, "buscar_form": buscar_form, })

def contacto(request):
    return render(request, 'contacto.html', { })

def consulta(request, organismo_id):
    organismo = obtener_organismo(organismo_id)
    if request.method == 'POST': #En caso de que se haya realizado una busqueda
        consulta_form = ConsultaForm(request.POST)
        if consulta_form.is_valid():
            consulta = consulta_form.save()
            #enviar email de validacion
            to_email = consulta_form.cleaned_data.get('email')#Obtenemos el correo
            #Preparamos el correo electronico
            mail_subject = 'Confirma tu correo de respuesta para Consultas Gubernamentales Jujuy.'
            mail_template= 'acc_active_email.html'
            message = render_to_string(mail_template, {
                    'consulta': consulta,
                    'token':account_activation_token.make_token(consulta),
                })
            #Instanciamos el objeto mail con destinatario
            email = EmailMessage(
                        mail_subject, message, to=[to_email]
            )
            #Enviamos el correo
            email.send()
            return render(request, 'consulta.html', {"organismo":organismo})
    else:
        consulta_form = ConsultaForm(initial={'organismo': organismo_id})
    return render(request, 'consulta.html', {"organismo":organismo, "consulta_form": consulta_form, })

def activate(request, consulta_id, token):
    try:
        consulta = Consulta.objects.get(pk=consulta_id)
    except(TypeError, ValueError, OverflowError, Consulta.DoesNotExist):
        consulta = None
    if consulta is not None and account_activation_token.check_token(consulta, token):
        consulta.valida = True
        consulta.save()
        return render(request, 'resultado.html', {'texto': 'Excelente! Su correo electronico fue validada.', })
    else:
        return render(request, 'resultado.html', {'texto': 'El link de activacion es invalido!', })