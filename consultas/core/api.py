#Para api
import requests 
import json
from django.core.cache import cache
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

#Definimos funcion de retry
def requests_retry_session(
    retries=3,
    backoff_factor=0.3,
    status_forcelist=(500, 502, 504),
    session=None,
):
    session = session or requests.Session()
    retry = Retry(
        total=retries,
        read=retries,
        connect=retries,
        backoff_factor=backoff_factor,
        status_forcelist=status_forcelist,
    )
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    return session

#Definimos objeto generico
class Object(object):
    pass

#Funciones API
def obtener_organismos():
    organismos = cache.get("organismos")
    if organismos is None:
        r = requests_retry_session().get('http://organigrama.jujuy.gob.ar/ws_org/')
        orgs = json.loads(r.text)['data']
        organismos = list()
        for org in orgs:
            organismos.append((org['id'],org['nombre']))
        cache.set("organismos", organismos)
    return organismos

def obtener_organismo(id_organismo):
    organismo = cache.get("org"+str(id_organismo))
    if organismo is None:
        print("Buscamos org" + str(id_organismo))
        r = requests_retry_session().get('http://organigrama.jujuy.gob.ar/ws_organismo/'+str(id_organismo))
        org = json.loads(r.text)['data']
        organismo = Object
        organismo.id = org['id']
        organismo.nombre = org['nombre']
        organismo.web = org['web']
        if org['icono']:
            organismo.icono = "http://organigrama.jujuy.gob.ar/" + org['icono']
        organismo.descripcion = org['descripcion']
        organismo.telefonos = org['telefonos']
        organismo.direccion = org['direccion']
        cache.set("org"+str(id_organismo), organismo)
    return organismo

def nombre_organismo(id_organismo):
    organismos = obtener_organismos()
    for org in organismos:
        if org[0] == id_organismo:
            return org[1]
    return "Sin Nombre"