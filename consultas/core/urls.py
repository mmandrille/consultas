from django.conf.urls import url
from django.urls import path
#Import personales
from . import views

app_name = 'core'
urlpatterns = [
    url(r'^$', views.home, name='home'),
    path('contacto/', views.contacto, name='contacto'),
    
    #Consultas
    path('<int:organismo_id>/', views.consulta, name='consulta'),
    #Mails de Activacion
    url(r'^activate/(?P<consulta_id>[0-9]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', views.activate, name='activate'),
]