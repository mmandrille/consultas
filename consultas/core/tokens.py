from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils import six
#Import Personales
from .models import Consulta

class TokenGenerator(PasswordResetTokenGenerator):
    def _make_hash_value(self, Consulta, timestamp):
        return (
            six.text_type(Consulta.pk) + six.text_type(timestamp) +
            six.text_type(Consulta.valida)
        )
account_activation_token = TokenGenerator()