#!/bin/bash
cd /opt/consultas
source venv/bin/activate
cd /opt/consultas/consultas
gunicorn consultas.wsgi -t 600 -b 127.0.0.1:8016 -w 6 --user=servidor --group=servidor --log-file=/opt/consultas/gunicorn.log 2>>/opt/consultas/gunicorn.log
